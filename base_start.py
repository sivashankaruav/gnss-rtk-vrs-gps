__author__ = 'Sivashankar, Research Engg, MetaRPAS Inc.,'
import time
import serial
import string
import pynmea2
import base64

lat = "37.187043215"#37.40058958""#36.6861326643"#37.4008177155"
lon = "126.665945551"#126.96554230"#126.766663431"#126.9660614633"
alt = "11.472"#69.222"#15.079"#82.7026"


sensors = [0 for a in range(7)]

ser = serial.Serial()
ser.port = "/dev/ttyAMA0"
ser.baudrate = 115200
ser.timeout = 1
ser.open()

ser.write(("FIX POSITION " +lat +" " +lon +" " +alt +"\r\n").encode())
ser.write(("ECUTOFF BD2 10.0\r\n").encode())
ser.write(("ECUTOFF GPS 10.0\r\n").encode())
ser.write(("ECUTOFF GLONASS 10.0\r\n").encode())
ser.write(("INTERFACEMODE COM2 AUTOMATIC AUTOMATIC ON\r\n").encode())
ser.write(("LOG COM2 RTCM1074 ONTIME 1\r\n").encode())
ser.write(("LOG COM2 RTCM1084 ONTIME 1\r\n").encode())
ser.write(("LOG COM2 RTCM1124 ONTIME 1\r\n").encode())
ser.write(("LOG COM2 RTCM1005 ONTIME 1\r\n").encode())
ser.write(("log com3 gpgga ontime 1 0 nohold\r\n").encode())

while True:
    #print(ser.readline())
    data = ser.readline()[:-2]
    data = data.decode("ascii", "ignore")
    try:
        gpgga = pynmea2.parse(data)
        s_type = gpgga.gps_qual
        if s_type == 0:
            sensors[1] = "Fix not available or invalid"
        elif s_type == 1:
            sensors[1] = "GPS SPS Mode, fix valid"
        elif s_type == 2:
            sensors[1] = "Differential GPS, SPS Mode, fix valid"
        elif s_type == 3:
            sensors[1] = "GPS PPS Mode, fix valid"
        elif s_type == 4:
            sensors[1] = "Real Time Kinematic (RTK)"
        elif s_type == 5:
            sensors[1] = "Float RTK"
        elif s_type == 6:
            sensors[1] = "Estimated (dead reckoning) Mode"
        elif s_type == 7:
            sensors[1] = "Manual Input Mode"
        else:
            sensors[1] = "GPS Error"
     
        sensors[0] = str(gpgga.timestamp)
        lat_deg = '%02d°%02d′%07.4f″' % (gpgga.latitude, gpgga.latitude_minutes, gpgga.latitude_seconds)
        sensors[2] = str(gpgga.latitude) + str(" / ") + str(lat_deg)
        log_deg = '%02d°%02d′%07.4f″' % (gpgga.longitude, gpgga.longitude_minutes, gpgga.longitude_seconds)
        sensors[3] = str(gpgga.longitude) + str(" / ") + str(log_deg)
        try:
            sensors[4] = str(gpgga.altitude) + str(" / ") + str((gpgga.altitude) + (float)(gpgga.geo_sep))
        except:
            continue
        sensors[5] = str(gpgga.num_sats)
        sensors[6] = str(data)
        print("Time : "+sensors[0])
        print("Soulution Type :" +sensors[1])
        print("Latitude : " +sensors[2])
        print("Longitude : " +sensors[3])
        print("Alt/Hig :" +sensors[4])
        print("Sat Count :" +sensors[5])
        print("NMEA :" +sensors[6])
    except:
        pass
            

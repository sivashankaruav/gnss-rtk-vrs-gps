import webiopi

from webiopi.devices.serial import Serial
serial = Serial("ttyAMA0", 115200)


HOUR_ON  = 8  # Turn Light ON at 08:00
HOUR_OFF = 18 # Turn Light OFF at 18:00

# setup function is automatically called at WebIOPi startup
def setup():
    
    # log Version
    if ((HOUR_ON == 8) and (HOUR_OFF == 18)):
        serial.writeString("log versiona once\r\n")
        #serial.write("Hi")

# loop function is repeatedly called by WebIOPi 
def loop():
    
    
        
    # gives CPU some time before looping again
    webiopi.sleep(1)

@webiopi.macro
def getLightHours():
    return "%d;%d" % (HOUR_ON, HOUR_OFF)

@webiopi.macro
def setLightHours(on, off):
    global HOUR_ON, HOUR_OFF
    HOUR_ON = int(on)
    HOUR_OFF = int(off)
    # Start Base
    if ((HOUR_ON == 5) and (HOUR_OFF == 5)):
        base_scr(str(HOUR_ON), str(HOUR_OFF))
        #serial.writeString("Base\r\n")

    # Start VRS
    if ((HOUR_OFF == 9) and (HOUR_ON == 9)):
        vrs_scr(str(HOUR_ON), str(HOUR_OFF))
        #serial.writeString("VRS\r\n")

    return getLightHours()

#OEM Base Script
def base_scr(lat,log):
    serial.writeString("fix " +lat +"," +log +"\r\n")
    

#OEM VRS Script
def vrs_scr(ip, port):
    serial.writeString("ip_address " +ip +":" +port +"\r\n")
    
    
# destroy function is called at WebIOPi shutdown
def destroy():
    serial.writeString("saveconfig\r\n")

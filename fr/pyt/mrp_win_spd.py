from gpiozero import DigitalInputDevice
from time import sleep
import math

count = 0
interval = 1		# How often to report speed

def calc_speed(time_sec):
    global count

    dist_ms = count * 0.0875
    return dist_ms
def spin():
    global count
    count = count + 1

wind_speed_sensor = DigitalInputDevice(5)
wind_speed_sensor.when_activated = spin

while 1:
    count = 0
    sleep(interval)
    print calc_speed(interval), "m/s"

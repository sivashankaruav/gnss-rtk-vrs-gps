_author__ = 'Sivashankar, Research Engg, MetaRPAS Inc.,'


import webiopi
import time
import serial
import string
import io
from pynmea import nmea
from webiopi.devices.serial import Serial


ser = serial.Serial()
ser.port = "/dev/ttyAMA0"
ser.baudrate = 115200
ser.timeout = 1
ser.open()
gpgga = nmea.GPGGA()
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser,1), encoding='ascii')

lat_tp = ""
log_tp = ""
sl_tp = ""

while 1:
    data = sio.readline()[:-1]
    if data[0:6] == '$GNGGA':
        ##method for parsing the sentence
        gpgga.parse(data)
        lats = gpgga.latitude
        longitude = gpgga.longitude
        s_type = gpgga.gps_qual
        lat_dd = float(lats[0:2])
        lat_mm = float(lats[2:])
        x1 = (lat_mm / 60)
        x2 = lat_dd + x1
        lat_tp = ("Latitude : " +str(x2))
        log_dd = float(longitude[0:3])
        log_mm = float(longitude[3:])
        y1 = (log_mm / 60)
        y2 = lat_dd + y1
        log_tp = ("Longitude : " +str(y2))
        if int(s_type) == 0:
                sl_tp = "Solution Type : Fix not available or invalid"
        elif int(s_type) == 1:
                sl_tp = "Solution Type : GPS SPS Mode, fix valid"
        elif int(s_type) == 2:
                sl_tp = "Solution Type : Differential GPS, SPS Mode, fix valid"
        elif int(s_type) == 3:
                sl_tp = "Solution Type : GPS PPS Mode, fix valid"
        elif int(s_type) == 4:
                sl_tp = "Solution Type : Real Time Kinematic (RTK)"
        elif int(s_type) == 5:
                sl_tp = "Solution Type : Float RTK"
        elif int(s_type) == 6:
                sl_tp = "Solution Type : Estimated (dead reckoning) Mode"
        elif int(s_type) == 7:
            sl_tp = "Solution Type : Manual Input Mode"
        else:
                sl_tp = "Solution Type : GPS Error"
    print sl_tp
    print lat_tp
    print log_tp

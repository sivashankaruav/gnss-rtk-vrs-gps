_author__ = 'Sivashankar, Research Engg, MetaRPAS Inc.,'
import time
import string
import io
from pynmea import nmea
import webiopi
from webiopi.devices.serial import Serial
from gpiozero import DigitalInputDevice
from time import sleep
import math

#serial = Serial("ttyAMA0", 115200)

count = 0
dist_ms = 0
interval = 1		# How often to report speed

def setup():
    wind_speed_sensor = DigitalInputDevice(5)
    wind_speed_sensor.when_activated = spin

def calc_speed(time_sec):
    global count

    dist_ms = count * 0.0875
    return dist_ms
def spin():
    global count
    count = count + 1    


def loop():
    count = 0
    sleep(interval)
    print calc_speed(interval), "m/s"             

    webiopi.sleep(1)

@webiopi.macro
def getValue():
    return "%f" % dist_ms



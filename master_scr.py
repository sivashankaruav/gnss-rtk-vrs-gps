__author__ = 'Sivashankar, Research Engg, MetaRPAS Inc.,'
import time
import serial
import string
import pynmea2
import base64
import socket
import sys

ser = serial.Serial()
ser.port = "/dev/ttyAMA0"
ser.baudrate = 115200
ser.timeout = 1
ser.open()
ser.flush()
ser.flushInput()
ser.flushOutput()
i = 0
sensors = [0 for a in range(7)]
def disp_info():
    """The Function is used to decode the recived nmea data from OEM"""
    data = ser.readline()[:-2]
    data = data.decode("ascii", "ignore")
    try:
        gpgga = pynmea2.parse(data)
        s_type = gpgga.gps_qual
        if s_type == 0:
            sensors[1] = "Fix not available or invalid"
        elif s_type == 1:
            sensors[1] = "GPS SPS Mode, fix valid"
        elif s_type == 2:
            sensors[1] = "Differential GPS, SPS Mode, fix valid"
        elif s_type == 3:
            sensors[1] = "GPS PPS Mode, fix valid"
        elif s_type == 4:
            sensors[1] = "Real Time Kinematic (RTK)"
        elif s_type == 5:
            sensors[1] = "Float RTK"
        elif s_type == 6:
            sensors[1] = "Estimated (dead reckoning) Mode"
        elif s_type == 7:
            sensors[1] = "Manual Input Mode"
        else:
            sensors[1] = "GPS Error"
     
        sensors[0] = str(gpgga.timestamp)
        lat_deg = '%02d°%02d′%07.4f″' % (gpgga.latitude, gpgga.latitude_minutes, gpgga.latitude_seconds)
        lat_nom = '%.10f' % gpgga.latitude
        sensors[2] = str(lat_nom) + str(" / ") + str(lat_deg)
        log_deg = '%02d°%02d′%07.4f″' % (gpgga.longitude, gpgga.longitude_minutes, gpgga.longitude_seconds)
        log_nom = '%.10f' % gpgga.longitude
        sensors[3] = str(log_nom) + str(" / ") + str(log_deg)
        try:
            alt_nom = '%.3f' % gpgga.altitude
            hgt_nom = '%.3f' % ((gpgga.altitude) + (float)(gpgga.geo_sep))
            sensors[4] = str(alt_nom) + str(" / ") + str(hgt_nom)
        except:
            continue
        sensors[5] = str(gpgga.num_sats)
        sensors[6] = str(data)
        print("Time : "+sensors[0])
        print("Soulution Type :" +sensors[1])
        print("Latitude : " +sensors[2])
        print("Longitude : " +sensors[3])
        print("Alt/Hig :" +sensors[4])
        print("Sat Count :" +sensors[5])
        print("NMEA :" +sensors[6])
    except:
        pass
    

def VRS_fun():
    
    """The function returns the lat, log and alt once the VRS got RTK fix solution"""
    
    GPS_Solution = 0
    i = 0

    username = "metarpas00" #username for RTCM correction service 
    password = "ngii" #password for RTCM correction service
    my_host = "vrs3.ngii.go.kr" #URL for the server
    port = 2101 #port for the service

    """'''Generate an encoding of the username:password for the service.
    The string must be first encoded in ascii to be correctly parsed by the
    base64.b64encode function.'''"""

    pwd = base64.b64encode("{}:{}".format(username, password).encode('ascii'))

    #The following decoding is necessary in order to remove the b' character that
    #the ascii encoding add. Othrewise said character will be sent to the net and misinterpreted.
    pwd = pwd.decode('ascii')

    header =\
    "GET /VRS-RTCM31 HTTP/1.1\r\n" +\
    "Host vrs3.ngii.go.kr\r\n" +\
    "Ntrip-Version: Ntrip/1.0\r\n" +\
    "User-Agent: ntrip1.py/0.1\r\n" +\
    "Accept: */*" +\
    "Connection: close\r\n" +\
    "Authorization: Basic {}\r\n\r\n".format(pwd)


    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((my_host,int(port)))
    s.send(header.encode('ascii'))
    resp = s.recv(1024).decode('ascii')

    while GPS_Solution != 4:
        data = ser.readline()[:-2]
        data = data.decode("ascii", "ignore")
        try:
            gpgga = pynmea2.parse(data)
            GPS_Solution = gpgga.gps_qual
            if GPS_Solution > 0:
                if i == 0:
                    dummyNMEA = str(data) + "\r\n"
                    s.send(dummyNMEA.encode('ascii'))
                    print("NO_RTK")
                    i = 1
            if i == 1:
                ser.write(s.recv(1024))
                sys.stdout.flush()
                print("NO_RTK")
            if GPS_Solution == 4:
                s.close()
                print("RTK_FIX")
                #return GPS_Solution, (gpgga.altitude + (float)(gpgga.geo_sep)), gpgga.latitude, gpgga.longitude#u-blox
                return GPS_Solution, gpgga.altitude, gpgga.latitude, gpgga.longitude#uni-com
            
        except:
            pass

def base_script(lat, lon, alt):
    """A one time run script for base operation"""
    lat = '%.10f' % lat
    lon = '%.10f' % lon
    alt = '%.3f' % alt
    ser.flush()
    ser.flushInput()
    ser.flushOutput()
    ser.write(("FIX POSITION " +str(lat) +" " +str(lon) +" " +str(alt) +"\r\n").encode())
    ser.write(("ECUTOFF BD2 10.0\r\n").encode())
    ser.write(("ECUTOFF GPS 10.0\r\n").encode())
    ser.write(("ECUTOFF GLONASS 10.0\r\n").encode())
    ser.write(("INTERFACEMODE COM2 AUTOMATIC AUTOMATIC ON\r\n").encode())
    ser.write(("LOG COM2 RTCM1074 ONTIME 1\r\n").encode())
    ser.write(("LOG COM2 RTCM1084 ONTIME 1\r\n").encode())
    ser.write(("LOG COM2 RTCM1124 ONTIME 1\r\n").encode())
    ser.write(("LOG COM2 RTCM1005 ONTIME 1\r\n").encode())
    ser.write(("log com3 gpgga ontime 1 0 nohold\r\n").encode())

while True:
    if i == 0:
        try:
            gps_sol, alt, lat, lon = VRS_fun()
            print(gps_sol)
            
        except:
            continue
        if gps_sol == 4:
            print("RTK_FIX")
            i =1
    elif i == 1:
        base_script(lat, lon, alt)
        print("Base_Started")
        i = 2
            
    else:
        disp_info()#dprint(ser.readline())
            
    
            

import base64
import socket

dummyNMEA = "GPGGA, 021233.00,3724.04054910,N,12657.92606557,E,1,07,4.3,46.1247,M,22.9395,M,00,0000*43"

username = "metarpas00" #username of RTCM Correction Server
password = "ngii" #password for RTCM Correction Server
my_host = "vrs3.ngii.go.kr" #URL for the server
port = 2101 #port for the server

"""'''Generate an encoding of the username:password for the service.
The string must be first encoded in ascii to be correctly parsed by the
base64.b64encoder function.'''"""

pwd = base64.b64encode("{}:{}".format(username, password).encode('ascii'))

#The following decoding is necessary in order to remove the b' character that
#the ascii encoding add. Otherwise said character will be sent to the net and misinterpreted.

pwd = pwd.decode('ascii')

print("Header sending...\n")

header =\
       "GET /VRS-RTCM31 HTTP/1.1\r\n" +\
       "Host my_host\r\n" +\
       "Ntrip-Version: Ntrip/1.0\r\n" +\
       "User-Agent: ntrip.py/0.1\r\n" +\
       "Accept: */*" +\
       "Connection: closer\r\n" +\
       "Authorization: Basic {}\r\n\r\n".format(pwd)


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((my_host,int(port)))
s.send(header.encode('ascii'))


data = s.recv(2048)#.decode('ascii')
print(data)
data = s.recv(2048)
print(data)
s.close()

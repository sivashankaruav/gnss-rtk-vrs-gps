import serial

ser = serial.Serial()
ser.port = "/dev/ttyAMA0"
ser.baudrate = 115200
ser.timeout = 1
ser.open()

while True:
    data = ser.readline()[:-2]
    data = data.decode("ascii", "ignore")
    print(data)
    

__author__ = 'Sivashankar, Research Engg, MetaRPAS Inc.,'
import time
import serial
import string
import pynmea2
import base64
import socket
import sys

sensors = [0 for a in range(7)]

username = "metarpas00" #username for RTCM correction service 
password = "ngii" #password for RTCM correction service
my_host = "vrs3.ngii.go.kr" #URL for the server
port = 2101 #port for the service

"""'''Generate an encoding of the username:password for the service.
The string must be first encoded in ascii to be correctly parsed by the
base64.b64encode function.'''"""

pwd = base64.b64encode("{}:{}".format(username, password).encode('ascii'))

#The following decoding is necessary in order to remove the b' character that
#the ascii encoding add. Othrewise said character will be sent to the net and misinterpreted.
pwd = pwd.decode('ascii')

header =\
"GET /VRS-RTCM31 HTTP/1.1\r\n" +\
"Host vrs3.ngii.go.kr\r\n" +\
"Ntrip-Version: Ntrip/1.0\r\n" +\
"User-Agent: ntrip1.py/0.1\r\n" +\
"Accept: */*" +\
"Connection: close\r\n" +\
"Authorization: Basic {}\r\n\r\n".format(pwd)


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((my_host,int(port)))
s.send(header.encode('ascii'))
resp = s.recv(1024).decode('ascii')
print(resp)

i = 0
ser = serial.Serial()
ser.port = "/dev/ttyAMA0"
ser.baudrate = 115200
ser.timeout = 1
ser.open()
ser.flush()
ser.flushInput()
ser.flushOutput()

while True:
        data = ser.readline()[:-2]
        data = data.decode("ascii", "ignore")
        try:
                gpgga = pynmea2.parse(data)
                s_type = gpgga.gps_qual
                if s_type == 0:
                        sensors[1] = "Fix not available or invalid"
                elif s_type == 1:
                        sensors[1] = "GPS SPS Mode, fix valid"
                elif s_type == 2:
                        sensors[1] = "Differential GPS, SPS Mode, fix valid"
                elif s_type == 3:
                        sensors[1] = "GPS PPS Mode, fix valid"
                elif s_type == 4:
                        sensors[1] = "Real Time Kinematic (RTK)"
                elif s_type == 5:
                        sensors[1] = "Float RTK"
                elif s_type == 6:
                        sensors[1] = "Estimated (dead reckoning) Mode"
                elif s_type == 7:
                        sensors[1] = "Manual Input Mode"
                else:
                        sensors[1] = "GPS Error"
                if s_type > 0:
                        if i == 0:
                                dummyNMEA = str(data) + "\r\n"
                                s.send(dummyNMEA.encode('ascii'))
                                i = 1
                                                    
                if i == 1:
                        ser.write(s.recv(1024))
                        sys.stdout.flush()
                sensors[0] = str(gpgga.timestamp)
                lat_deg = '%02d°%02d′%07.4f″' % (gpgga.latitude, gpgga.latitude_minutes, gpgga.latitude_seconds)
                lat_nom = '%.10f' % gpgga.latitude
                sensors[2] = str(lat_nom) + str(" / ") + str(lat_deg)
                log_deg = '%02d°%02d′%07.4f″' % (gpgga.longitude, gpgga.longitude_minutes, gpgga.longitude_seconds)
                log_nom = '%.10f' % gpgga.longitude
                sensors[3] = str(log_nom) + str(" / ") + str(log_deg)
                try:
                        alt_nom = '%.3f' % gpgga.altitude
                        hgt_nom = '%.3f' % ((gpgga.altitude) + (float)(gpgga.geo_sep))
                        sensors[4] = str(alt_nom) + str(" / ") + str(hgt_nom)
                except:
                        continue
                sensors[5] = str(gpgga.num_sats)
                sensors[6] = str(data)
                print("Time : "+sensors[0])
                print("Soulution Type :" +sensors[1])
                print("Latitude : " +sensors[2])
                print("Longitude : " +sensors[3])
                print("Alt/Hig :" +sensors[4])
                print("Sat Count :" +sensors[5])
                print("NMEA :" +sensors[6])
        except:
                pass
        
        """
                ser.flush()
                ser.flushInput()
                ser.flushOutput()"""
        """
        ser.write(("log Version\r\n").encode())
        DAT = ser.readline().decode("ascii", "ignore")    
        print()
        time.sleep(0.5)
        ser.write(("INTERFACEMODE COM3 RTCMV3 UNICORE ON\r\n").encode())
        DAT += ser.readline().decode("ascii", "ignore")    
        print(DAT)
        #time.sleep(1)
        print(DAT)
        ser.write(("LOG COM3 GPGGA ONTIME 1 0 NOHOLD\r\n").encode())
        DAT += ser.readline().decode("ascii", "ignore")    
        print(DAT)
        #time.sleep(1)
        print(DAT)
        time.sleep(90)"""




                    

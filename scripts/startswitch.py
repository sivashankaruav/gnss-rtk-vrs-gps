# start script for Raspberry Pi
# watch level on pin 5 to select mode

import RPi.GPIO as GPIO
import os


GPIO.setmode(GPIO.BCM)
GPIO.setup(8, GPIO.IN)

GPIO.setup(24, GPIO.OUT)
GPIO.output(24, True)
GPIO.setup(18, GPIO.OUT)
GPIO.output(18, True)


if (GPIO.input(8)):
   #GPIO.output(24, True)
   #GPIO.output(18, True)
   os.system("sudo blink &")
   os.system("sudo /home/pi/RTK/workdir/start-base.sh")
else:
   GPIO.output(24, False)
   GPIO.output(18, False)
   os.system("sudo /home/pi/RTK/workdir/start-ppp.sh")

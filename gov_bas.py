'''
HTTP/1.1 401 Unauthorized
WWW-Authenticate: Basic realm="VRS-RTCM31"

Soulution Type :Real Time Kinematic (RTK)
Latitude : 36.5208149515 / 36°31′14.9338″
Longitude : 126.9360906315 / 126°56′09.9263″
Alt/Hig :146.2321 / 169.823
Sat Count :14
NMEA :$GNGGA,054840.00,3631.24889709,N,12656.16543789,E,4,14,0.8,146.2321,M,23.5909,M,03,0492*7D
Time : 05:48:41
Soulution Type :Float RTK
Latitude : 36.520816020666665 / 36°31′14.9377″
Longitude : 126.9360893 / 126°56′09.9215″
Alt/Hig :146.4193 / 170.0102
Sat Count :14
NMEA :$GNGGA,054841.00,3631.24896124,N,12656.16535800,E,5,14,0.8,146.4193,M,23.5909,M,03,0492*78

'''

__author__ = 'Sivashankar, Research Engg, MetaRPAS Inc.,'
import time
import serial
import string
import pynmea2
import base64

lat = "37.4007325711"
lon = "126.9662143256"
alt = "157.265"


sensors = [0 for a in range(7)]

ser = serial.Serial()
ser.port = "/dev/ttyAMA0"
ser.baudrate = 115200
ser.timeout = 1
ser.open()

ser.write(("FIX POSITION " +lat +" " +lon +" " +alt +"\r\n").encode())
ser.write(("ECUTOFF BD2 10.0\r\n").encode())
ser.write(("ECUTOFF GPS 10.0\r\n").encode())
ser.write(("ECUTOFF GLONASS 10.0\r\n").encode())
ser.write(("INTERFACEMODE COM2 AUTOMATIC AUTOMATIC ON\r\n").encode())
ser.write(("LOG COM2 RTCM1074 ONTIME 1\r\n").encode())
ser.write(("LOG COM2 RTCM1084 ONTIME 1\r\n").encode())
ser.write(("LOG COM2 RTCM1124 ONTIME 1\r\n").encode())
ser.write(("LOG COM2 RTCM1005 ONTIME 1\r\n").encode())
ser.write(("log com3 gpgga ontime 1 0 nohold\r\n").encode())

while True:
    print(ser.readline())
    data = ser.readline()[:-2]
    data = data.decode("ascii", "ignore")
    try:
        gpgga = pynmea2.parse(data)
        s_type = gpgga.gps_qual
        if s_type == 0:
            sensors[1] = "Fix not available or invalid"
        elif s_type == 1:
            sensors[1] = "GPS SPS Mode, fix valid"
        elif s_type == 2:
            sensors[1] = "Differential GPS, SPS Mode, fix valid"
        elif s_type == 3:
            sensors[1] = "GPS PPS Mode, fix valid"
        elif s_type == 4:
            sensors[1] = "Real Time Kinematic (RTK)"
        elif s_type == 5:
            sensors[1] = "Float RTK"
        elif s_type == 6:
            sensors[1] = "Estimated (dead reckoning) Mode"
        elif s_type == 7:
            sensors[1] = "Manual Input Mode"
        else:
            sensors[1] = "GPS Error"
     
        sensors[0] = str(gpgga.timestamp)
        lat_deg = '%02d°%02d′%07.4f″' % (gpgga.latitude, gpgga.latitude_minutes, gpgga.latitude_seconds)
        lat_nom = '%.10f' % gpgga.latitude
        sensors[2] = str(lat_nom) + str(" / ") + str(lat_deg)
        log_deg = '%02d°%02d′%07.4f″' % (gpgga.longitude, gpgga.longitude_minutes, gpgga.longitude_seconds)
        log_nom = '%.10f' % gpgga.longitude
        sensors[3] = str(log_nom) + str(" / ") + str(log_deg)
        try:
            alt_nom = '%.3f' % gpgga.altitude
            hgt_nom = '%.3f' % ((gpgga.altitude) + (float)(gpgga.geo_sep))
            sensors[4] = str(alt_nom) + str(" / ") + str(hgt_nom)
        except:
            continue
        sensors[5] = str(gpgga.num_sats)
        sensors[6] = str(data)
        print("Time : "+sensors[0])
        print("Soulution Type :" +sensors[1])
        print("Latitude : " +sensors[2])
        print("Longitude : " +sensors[3])
        print("Alt/Hig :" +sensors[4])
        print("Sat Count :" +sensors[5])
        print("NMEA :" +sensors[6])
    except:
        pass
            
